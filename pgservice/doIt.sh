#!/bin/bash
#
# Starts the playbook.
#
# @version 2023-11-18
# @author lp


ansible-playbook --ask-vault-pass playbooks/$1.yaml -e "DB='$2'" -i ../inventory.yaml -u admin
