#!/bin/bash
#
# Copy DB
#
# @version 2025-01-22
# @author lp

tempfile=/tmp/copydb_$PPID.data

echo Dumping...
pg_dump puc_master -c \
-U postgres >$tempfile

echo Changing ownership...
sed -i "s/OWNER TO puc_master/OWNER TO {{DB}}/g" $tempfile

echo Importing...
psql {{DB}} -U postgres -a <$tempfile

echo Anonymizing users...
psql {{DB}} -U postgres \
-c "update users set loginname='mcmaster',firstname='MC',lastname='MC',email='mc@mc.de',flags=0 where id=1;" \
-c "update users set loginname='mcreader',firstname='MC',lastname='MC',email='mc@mc.de',flags=0 where id=2;" \
-c "update users set loginname='mcwriter',firstname='MC',lastname='MC',email='mc@mc.de',flags=0 where id=3;" \
-c "update users set loginname='mcother'||id,firstname='MC',lastname='MC',email='mc@mc.de',flags=1 where id>3;" \
-c "delete from passwords where id not in (select max(id) from passwords group by user_id);" \
-c "update passwords set password='\$2b\$04\$QDa2OyyvQUvBaS2ubVntcewurTXnDdhTjjeDiZw073Og7KxB.DIYS', salt='KYiiVYG2xb4MW3Ce' where user_id=0;" \
-c "update passwords set password='\$2b\$04\$ayCzcSzHYkjFPECyTDjgQuLvyx5kLzkDqrnKSZc6n57OjXkmkqA7C', salt='X0ul0wkT1Ud7GRmb' where user_id=1;" \
-c "update passwords set password='\$2b\$04\$QVGzYF.0axbtTDbfTlPUM.zZcY2G6/rDbeLCeqZCMtHX0GWdWTFje', salt='UOclxYbxiiSMyzUg' where user_id=2;" \
-c "update passwords set password='\$2b\$04\$ZUa1YiTza03tPkv0USu2R.zQBG3brPRC2lT3I.iLM8LJh7YN3j1jC', salt='dc1VPOWawzyElyQu' where user_id=3;" \
-c "update passwords set password='\$2b\$04\$bCvfSBjqLhHUQUbJZDDvLOfQBVferrKkr8u8JbJglUBdWQ1Uc4mLa', salt='0ppOEIlmwibt3GU1' where user_id>3;"

echo cleanup...
rm $tempfile
