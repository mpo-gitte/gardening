#!/bin/bash

# Some initialization stuff
#
# @version 2024-11-03
# @author lp

#pip install -U pip
#pip install psycopg2-binary

ansible-galaxy collection install community.postgresql
ansible-galaxy collection install containers.podman
ansible-galaxy collection install ansible.posix
