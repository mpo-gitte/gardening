#!/bin/bash
#
# Encrypts a string.
#
# @version 2022-04-14
# @author pilgrim.lutz@imail.de

ansible-vault encrypt_string --ask-vault-pass "$2" --name "$1"
