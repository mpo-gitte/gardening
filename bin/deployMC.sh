#!/bin/bash
#
# Deployment script for MC.
#
# @version 2024-11-22
# @author lp
#
# $1: Stage: new, fix, master
# $2: Version number of MC to deploy
# $2: Branch: Branch of gardening project

# If you are on a X86 set environment variable HOSTTYPE to "amd64"!

dockreg=joe:49153
gitrep=ssh://git@joe.fritz.box:222/lutz/gardening.git

now=`date +%Y%m%d%H%M%s`
workdir=`mktemp -d /tmp/builder.${now}.XXXXXXXXX`

echo deploying stage $1 version $2

git clone ${gitrep} $workdir
cd $workdir

if [ -z "$3" ]
then
 branch=master
else
 branch=$3
fi

git checkout $branch

export DOCKER_REGISTRY=${dockreg}

ansible-playbook --ask-vault-pass mcinkube/playbooks/mc.yaml -e "mcstage=mc-$1" -e "mcversion=$2" -i inventory.yaml -u root
