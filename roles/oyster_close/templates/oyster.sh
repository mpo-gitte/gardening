#!/bin/bash

{% for con in oyster_open %}
iptables -A INPUT -p tcp {% if con.addr != "" %}-s {{con.addr}}{% endif %} --dport {{con.port}} -j ACCEPT
{% endfor %}
iptables -P INPUT DROP
