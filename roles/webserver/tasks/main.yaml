#
# Role for running Apache httpd
#
# @version 2025-02-06
# @author lp

---

- name: Ensure "{{webserver_remworkdir}}" exists
  when: webserver_reinit == true
  file: path="{{webserver_remworkdir}}" state=directory



- name: Stop service
  when: webserver_reinit == true
  ignore_errors: true
  become: yes
  service:
    name: "container-{{webserver_apache_containername}}"
    state: stopped



- name: Stop container
  when: webserver_reinit == true
  ignore_errors: true
  containers.podman.podman_container:
    name: "{{webserver_apache_containername}}"
    state: stopped



- name: Remove container
  when: webserver_reinit == true
  ignore_errors: true
  containers.podman.podman_container:
    name: "{{webserver_apache_containername}}"
    state: absent



- name: Run new container
  when: webserver_reinit == true
  containers.podman.podman_container:
    name: "{{webserver_apache_containername}}"
    image: "{{webserver_apache_containerimage}}"
    ports:
      - "80:{{webserver_apache_exposedport}}"
      - "443:{{webserver_apache_exposedport_ssl}}"
    state: started
    restart_policy: always



- name: Install additional software in container
  when: webserver_reinit == true
  shell:
    cmd: podman exec {{webserver_apache_containername}} apt-get update
  register: out

- name: Install additional software in container
  when: webserver_reinit == true
  shell:
    cmd: podman exec {{webserver_apache_containername}} apt-get install -y {{item}}
  with_items:
    - imagemagick
    - curl
  register: out



- name: Put index.html & co to workdir
  when: webserver_reinit == true
  template:
    src: "{{item}}"
    dest: "{{webserver_remworkdir}}"
    owner: root
    group: root
  with_items: "{{webserver_cgi_scripts + webserver_conffiles + webserver_sitefiles + webserver_otherfiles}}"
  register: out



- name: Put index.html & co to container
  when: webserver_reinit == true
  shell:
    cmd: podman cp {{webserver_remworkdir}}/{{item}} {{webserver_apache_containername}}:htdocs
  with_items: "{{webserver_sitefiles}}"
  register: out



- name: Put policy.xml for ImageMagic to container
  when: webserver_reinit == true
  shell:
    cmd: podman cp {{webserver_remworkdir}}/policy.xml {{webserver_apache_containername}}:/etc/ImageMagick-6
  register: out



- name: Save httpd.conf
  when: webserver_reinit == true
  stat:
    path: "{{webserver_remworkdir}}/httpd.conf"
  register: out

- name: Save httpd.conf
  when: webserver_reinit == true  and  out.stat.exists == false
  shell:
    cmd: podman cp {{webserver_apache_containername}}:conf/httpd.conf {{webserver_remworkdir}}
  register: out



- name: Put conf files to container
  when: webserver_reinit == true
  shell:
    cmd: podman cp {{webserver_remworkdir}}/{{item}} {{webserver_apache_containername}}:conf
  with_items: "{{webserver_conffiles}}"
  register: out



- name: Restore httpd.conf
  when: webserver_reinit == true
  shell:
    cmd: podman cp {{webserver_remworkdir}}/httpd.conf {{webserver_apache_containername}}:conf
  register: out



- name: Configuring Apache modules
  when: webserver_reinit == true
  shell:
    cmd: podman exec {{webserver_apache_containername}} bash -c "echo LoadModule {{item}} >>conf/httpd.conf"
  with_items:
    - proxy_module modules/mod_proxy.so
    - proxy_http_module modules/mod_proxy_http.so
    - slotmem_shm_module modules/mod_slotmem_shm.so
    - cache_module modules/mod_cache.so
    - cache_disk_module modules/mod_cache_disk.so
    - socache_shmcb_module modules/mod_socache_shmcb.so
    - rewrite_module modules/mod_rewrite.so
    - cgid_module modules/mod_cgid.so
    - substitute_module modules/mod_substitute.so
    - ssl_module modules/mod_ssl.so
  register: out



- name: Configuring Apache conf
  when: webserver_reinit == true
  shell:
    cmd: podman exec {{webserver_apache_containername}} bash -c "echo Include conf/{{item}} >>conf/httpd.conf"
  with_items: "{{webserver_conffiles}}"
  register: out



- name: Set server name
  when: webserver_reinit == true
  shell:
    cmd: podman exec {{webserver_apache_containername}} bash -c "echo ServerName {{webserver_servername}} >>conf/httpd.conf"
  register: out



- name: Create modstatuspasswd
  when: webserver_reinit == true
  delegate_to: localhost
  become: false
  shell:
    cmd:
      htpasswd -nb {{webserver_modstatus_user}} {{webserver_modstatus_password}}
  register: out

- name: Put modstatuspasswd to workdir
  when: webserver_reinit == true
  template:
    src: "modstatuspasswd"
    dest: "{{webserver_remworkdir}}"
    owner: root
    group: root

- name: Put modstatuspasswd files to container
  when: webserver_reinit == true
  shell:
    cmd: podman cp {{webserver_remworkdir}}/modstatuspasswd {{webserver_apache_containername}}:conf
  register: out



- name: Preparing cgi folder
  when: webserver_reinit == true
  ignore_errors: true
  containers.podman.podman_container_exec:
    name: "{{webserver_apache_containername}}"
    command: "mkdir MC"
    workdir: /usr/local/apache2/cgi-bin

- name: Put cgi files to container
  when: webserver_reinit == true
  shell:
    cmd: podman cp {{webserver_remworkdir}}/{{item}} {{webserver_apache_containername}}:cgi-bin/MC
  with_items: "{{webserver_cgi_scripts}}"
  register: out

- name: Set access rights for cgi scripts
  when: webserver_reinit == true
  containers.podman.podman_container_exec:
    name: "{{webserver_apache_containername}}"
    command: "chmod ugo+x {{item}}"
    workdir: /usr/local/apache2/cgi-bin/MC
  with_items: "{{webserver_cgi_scripts}}"
  register: out



- name: Preparing cache folder
  when: webserver_reinit == true
  ignore_errors: true
  containers.podman.podman_container_exec:
    name: "{{webserver_apache_containername}}"
    command: "mkdir {{webserver_apache_cachefolder}}"
    workdir: /

- name: Set owner for cache folder
  when: webserver_reinit == true
  containers.podman.podman_container_exec:
    name: "{{webserver_apache_containername}}"
    command: "chown www-data {{webserver_apache_cachefolder}}"

- name: Set access rights for cache folder
  when: webserver_reinit == true
  containers.podman.podman_container_exec:
    name: "{{webserver_apache_containername}}"
    command: "chmod ugo+x {{webserver_apache_cachefolder}}"



- name: Create cert serial number file
  when: webserver_reinit == true
  template:
    src: cert_serialnumber.inf
    dest: "{{webserver_remworkdir}}"
    owner: root
    group: root
  register: out



- name: Create certificate
  when: webserver_reinit == true
  shell:
    cmd: |
      openssl genrsa -out {{webserver_remworkdir}}/{{webserver_cert_filename}}.key 3072;
      openssl req -new -out {{webserver_remworkdir}}/website.csr -sha256 -key {{webserver_remworkdir}}/{{webserver_cert_filename}}.key <<-'EOF'
      {{webserver_cert_countrycode}}
      {{webserver_cert_province}}
      {{webserver_cert_city}}
      {{webserver_cert_company}}
      {{webserver_cert_unit}}
      {{webserver_cert_name}}
      {{webserver_cert_email}}
      
      
      EOF
      openssl x509 -req -in {{webserver_remworkdir}}/website.csr -days 365 -signkey {{webserver_remworkdir}}/{{webserver_cert_filename}}.key -outform PEM -out {{webserver_remworkdir}}/{{webserver_cert_filename}}.crt;
  register: out
  changed_when: '"writing new private key" in out.stderr'



- name: Get certificate fingerprint
  when: webserver_reinit == true
  shell:
    cmd: openssl x509 -noout -fingerprint -sha256 -inform pem -in {{webserver_remworkdir}}/{{webserver_cert_filename}}.crt
  register: getfingerprintout
- name: Write certificate fingerprint
  when: getfingerprintout.changed
  become: false
  local_action:
    copy content="{{getfingerprintout.stdout}}" dest="../cert_fingerprint.inf"



- name: Put cert to container
  when: webserver_reinit == true
  shell:
    cmd: podman cp {{webserver_remworkdir}}/{{item}} {{webserver_apache_containername}}:conf
  with_items:
    - "{{webserver_cert_filename}}.crt"
    - "{{webserver_cert_filename}}.key"
  register: out



- name: Stop container
  when: webserver_reinit == true
  ignore_errors: true
  containers.podman.podman_container:
    name: "{{webserver_apache_containername}}"
    state: stopped



- name: Put service file
  when: webserver_reinit == true
  template:
    src: httpd.service
    dest: "/etc/systemd/system/container-{{webserver_apache_containername}}.service"
    owner: root
    group: root



- name: systemd reload
  when: webserver_reinit == true
  become: yes
  systemd:
    daemon_reload: yes



- name: Start service
  when: webserver_reinit == true
  become: yes
  service:
    name: "container-{{webserver_apache_containername}}"
    enabled: yes
    state: started



- name: Open port
  set_fact:
    oyster_open: "{{ oyster_open + [{'addr': '192.168.1','port': 80 }, {'addr': '192.168.1','port': 443 }]}}"
