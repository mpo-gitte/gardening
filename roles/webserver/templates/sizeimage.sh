#!/bin/bash
#
# sizeimage.sh
#
# @verion 2024-05-27
# @author pilgrim.lutz@imail.de



#function log {
#
# echo `date "+%Y.%m.%d-%H:%M:%S %Z"` "[$1]" "$2" 1>>/var/log/sizeimage.sh.log
#
#}



export width=`echo $QUERY_STRING}|sed -e "s/.*\(width=\)\([0-9]*\).*/\2/"`
export height=`echo $QUERY_STRING}|sed -e "s/.*\(height=\)\([0-9]*\).*/\2/"`
export mode=`echo $QUERY_STRING}|sed -e "s/.*\(mode=\)\([0-9]*\).*/\2/"`
export name=`echo $QUERY_STRING|sed -e "s/.*\(name=\)\([0-9]*\.jpg\).*/\2/"`

# ToDo: Make content type depending on extension.
echo "Content-type: image/jpeg"
#echo "Content-type: text/html"
echo "Cache-Control: max-age=31536000,public"
echo ""

if [ $mode -ne 1 ]; then
 if [ $height -gt 150 ]; then
  export height=150
 fi
 if [ $width -gt 150 ]; then
  export width=150
 fi
fi

export ps=`expr $height / 5`

#log "info" "name=${name} mode=${mode} height=${height} width=${width}"

if [ $mode -eq 1 ]; then
 curl {{webserver_coverpictures_source}}/${name}|convert -resize ${width}x${height} - -
else
 curl {{webserver_coverpictures_source}}/${name}|convert -resize ${width}x${height} -gravity South -font Helvetica -pointsize $ps -draw "text 0,0 'PuC'" - -
fi
