#!/bin/bash
#
# mcauth.sh
#
# @verion 2022-09-01
# @author pilgrim.lutz@imail.de



function log {

 echo `date "+%Y.%m.%d-%H:%M:%S %Z"` "[$1]" "$2" 1>>/var/log/mcauth.sh.log

}



while read line
do
#  echo "$(date) $line" >> /tmp/mcauth.log;

  str=${line//&/;}
  array=(${str//;/ })

  host=""
  context=""
  mode=0
  jsessionid=""

  for i in "${!array[@]}"
  do
   p="${array[i]}"

   if [ $i -eq 0 ]; then
    param=(${p//// })
    host="${param[0]}"
    context="${param[1]}"
   fi

   param=(${p//=/ })

   if [ "${param[0]}" = "mode" ]; then
    mode="${param[1]}"
   fi
   if [ "${param[0]}" = "JSESSIONID" ]; then
    jsessionid="${param[1]}"
   fi
  done

  log "info" "Calling MC with: mode=$mode jsessionid=$jsessionid host=$host context=$context"

  ret=""

  if [ "$jsessionid" != "" ]; then
   rights=`curl -s -X GET -w "%{http_code} " --header "X-FORWARDED-FOR: {{webserver_xforwardedfor}}" --data-urlencode "sessionid=$jsessionid" -G "http://${host}/${context}/services/rights/forSessionId.vm"`

   log "info" "MC replied: $rights"

   may=`echo "$rights"|grep "\"seeCoverpictures\":.*true"`
   if [ "$may" != "" ]; then
    if [ $mode -ne 1 ]; then
     ret="ok";
    else
     may=`echo "$rights"|grep "\"seeCoverpicturesUnlimited\":.*true"`
     if [ "$may" != "" ]; then
      ret="ok";
     fi
    fi
   fi
  fi

# echo "$(date) ret: $ret" >> /tmp/mcauth.log;

 echo "$ret"
done < "${1:-/dev/stdin}"
