#!/bin/bash
#
# Starts the playbook.
#
# @version 2025-01-11
# @author pilgrim.lutz@imail.de


if [ "$2" != "" ]
then
stage="mcstage='$2'"
fi
if [ "$3" != "" ]
then
version="mcversion='$3'"
fi

ansible-playbook --ask-vault-pass playbooks/$1.yaml -e "$stage" -e "$version" -i ../inventory.yaml -u root
