#!/bin/bash
#
# Deploys MC
#
# @version 2021-02-06
# @author pilgrim.lutz@imail.de



if [ "$1" != "" ]
then
tag=$1
else
tag="all"
fi
if [ "$2" != "" ]
then
stage="mcstage='$2'"
else
stage="stage=''"
fi
echo tag=$tag
echo stage=$stage
ansible-playbook --ask-vault-pass playbook.yaml  -e "$stage" -i inventory.yaml -u root --tags "$tag"
